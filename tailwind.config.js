/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ], mode: "jit",
  //darkMode: false,
  theme: {
    extend: {
      zIndex: {
        100: 100,
      },

      keyframes: {
        show: {
          "0%, 49.99%": {opacity: 0, "z-index": 10},
          "50%, 100%": {opacity: 1, "z-index": 50},
        },
      },

      animation: {
        show: "show 0.7s",
      },
    },
  },
  plugins: [],
}
