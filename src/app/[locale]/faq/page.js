"use client";
export default function page() {
  return (
    <div className="container content-space-t-1 content-space-t-lg-3 content-space-b-2">
      <div className="w-md-75 w-lg-50 text-center mx-md-auto">
        <h1>FAQ</h1>
        <p>Search our FAQ for answers to anything you might ask.</p>
      </div>
      <div className="d-grid gap-3">
        <h2>Subscription</h2>

          <div className="accordion accordion-flush accordion-lg" id="accordionFAQBasics">

            <div className="accordion-item">
              <div className="accordion-header" id="headingBasicsOne">
                <a className="accordion-button" role="button" data-bs-toggle="collapse" data-bs-target="#collapseBasicsOne"
                   aria-expanded="true" aria-controls="collapseBasicsOne">
                  What methods of payments are supported?
                </a>
              </div>
              <div id="collapseBasicsOne" className="accordion-collapse collapse show" aria-labelledby="headingBasicsOne"
                   data-bs-parent="#accordionFAQBasics">
                <div className="accordion-body">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor,
                  dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a,
                  semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat.
                </div>
              </div>
            </div>


            <div className="accordion-item">
              <div className="accordion-header" id="headingBasicsTwo">
                <a className="accordion-button collapsed" role="button" data-bs-toggle="collapse"
                   data-bs-target="#collapseBasicsTwo" aria-expanded="false" aria-controls="collapseBasicsTwo">
                  Can I cancel at anytime?
                </a>
              </div>
              <div id="collapseBasicsTwo" className="accordion-collapse collapse" aria-labelledby="headingBasicsTwo"
                   data-bs-parent="#accordionFAQBasics">
                <div className="accordion-body">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor,
                  dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a,
                  semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat.
                </div>
              </div>
            </div>


            <div className="accordion-item">
              <div className="accordion-header" id="headingBasicsThree">
                <a className="accordion-button collapsed" role="button" data-bs-toggle="collapse"
                   data-bs-target="#collapseBasicsThree" aria-expanded="false" aria-controls="collapseBasicsThree">
                  How do I get a receipt for my purchase?
                </a>
              </div>
              <div id="collapseBasicsThree" className="accordion-collapse collapse" aria-labelledby="headingBasicsThree"
                   data-bs-parent="#accordionFAQBasics">
                <div className="accordion-body">
                  You'll receive an email from Bootstrap themes once your purchase is complete.
                </div>
              </div>
            </div>


            <div className="accordion-item">
              <div className="accordion-header" id="headingBasicsFour">
                <a className="accordion-button collapsed" role="button" data-bs-toggle="collapse"
                   data-bs-target="#collapseBasicsFour" aria-expanded="false" aria-controls="collapseBasicsFour">
                  Which license do I need?
                </a>
              </div>
              <div id="collapseBasicsFour" className="accordion-collapse collapse" aria-labelledby="headingBasicsFour"
                   data-bs-parent="#accordionFAQBasics">
                <div className="accordion-body">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor,
                  dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a,
                  semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat.
                </div>
              </div>
            </div>


            <div className="accordion-item">
              <div className="accordion-header" id="headingBasicsFive">
                <a className="accordion-button collapsed" role="button" data-bs-toggle="collapse"
                   data-bs-target="#collapseBasicsFive" aria-expanded="false" aria-controls="collapseBasicsFive">
                  How do I get access to a theme I purchased?
                </a>
              </div>
              <div id="collapseBasicsFive" className="accordion-collapse collapse" aria-labelledby="headingBasicsFive"
                   data-bs-parent="#accordionFAQBasics">
                <div className="accordion-body">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor,
                  dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a,
                  semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat.
                </div>
              </div>
            </div>


            <div className="accordion-item">
              <div className="accordion-header" id="headingBasicsSix">
                <a className="accordion-button collapsed" role="button" data-bs-toggle="collapse"
                   data-bs-target="#collapseBasicsSix" aria-expanded="false" aria-controls="collapseBasicsSix">
                  Upgrade License Type
                </a>
              </div>
              <div id="collapseBasicsSix" className="accordion-collapse collapse" aria-labelledby="headingBasicsSix"
                   data-bs-parent="#accordionFAQBasics">
                <div className="accordion-body">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor,
                  dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a,
                  semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat.
                </div>
              </div>
            </div>

          </div>
      </div>
    </div>
  )
}
