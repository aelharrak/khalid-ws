export default function RootLayout({children, params}) {
  return (
    <html>
      <body>
        <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
      {children}
    </div>
      </body>
    </html>
  );
}
