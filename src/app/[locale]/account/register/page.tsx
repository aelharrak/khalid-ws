"use client"
import {useForm, SubmitHandler} from "react-hook-form"


/*enum GenderEnum {
  female = "female",
  male = "male",
  other = "other",
}*/

interface IFormInput {
  login: string
  password: string // GenderEnum
  rePassword: string
}

export default function Index() {

  const {register,     formState: { errors }, handleSubmit} = useForm<IFormInput>()
  const onSubmit: SubmitHandler<IFormInput> = (data) => console.log(data)

  return (
    <div className="card">
      <h4 className="card-header">Login</h4>
      <div className="card-body">
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="form-group">
            <label>Username</label>
            <input
              {...register("login", {required: "Email Address is required", pattern: /^[A-Za-z]+$/i, maxLength: 20})}
              aria-invalid={errors.login ? "true" : "false"}
              type="text"/>
          </div>
          {errors.login && <p role="alert">{errors.login.message}</p>}

          <div className="form-group">
            <label>Password</label>
            <input  {...register("password")} type="password" className={`form-control`}/>
          </div>

          <div className="form-group">
            <label>Re Password</label>
            <input {...register("rePassword")} type="password" className={`form-control`}/>
          </div>

          <button className="btn btn-primary">
            Login
          </button>
          <a href="/account/register" className="btn btn-link">Register</a>
        </form>
      </div>
    </div>
  )
}
