'use client'
import {images} from './lib/images'
import Image from "next/image";

import {Splide, SplideSlide} from '@splidejs/react-splide';
import * as React from "react";
// Default theme
import '@splidejs/react-splide/css';
import useDevice from 'usedevice';

export default function Page() {
  const device = {};
  // Example of using navigator in a client-side check
  if (typeof useDevice() !== 'undefined') {
    const device = useDevice().device // client side
    console.log(device)
  }
  return (
    <Splide aria-label="My Favorite Images">
      {images.map((image, index) => (
        <SplideSlide key={index}>
          <Image
            sizes="100vw" style={{
            width: '100%',
            height: device === 'phone' ? '450' : '100',
          }}
            width={500}
            height={device === 'phone' ? '450' : '100'}
            src={image.src}
            alt={image.index}/>
        </SplideSlide>

      ))}
    </Splide>
  )
}



