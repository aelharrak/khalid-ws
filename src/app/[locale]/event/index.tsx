import React from 'react';
import event from "/public/event_04.jpg";
import Image from "next/image";
import {useTranslations} from "next-intl";

export default function index({title, bgColorCssClass}) {
  const t = useTranslations("Index");

  return (
    <section className={bgColorCssClass}>
      <div className="container events-two">
        <div className="section-header row px-2">
          <h3>{t(title)}</h3>
          <p>we are pleased to announce that we are creative some valuable events for our nation.</p>
        </div>
        <div className="event-items row ">
          <div className="col-12 event-item">
            <div className="image">
              <a href="event-single.html"><Image
                alt="khalid ben"
                src={event} className="img-fluid"
                placeholder="blur"
                quality={100} priority
                style={{
                  minWidth: "100%"
                }}
              /></a>
            </div>
            <div className="time">
              <h2>24</h2>
              <span>. . .</span>
              <p>April</p>
            </div>
            <div className="content">
              <h3>Online Learning and Earning Guidline</h3>
              <ul>
                <li><i className="fa fa-clock-o" aria-hidden="true"></i> 8:00 am</li>
                <li><i className="fa fa-map-marker" aria-hidden="true"></i> New Elefent Road, Dhaka,
                  Bangladesh
                </li>
              </ul>
              <p>Distinctive facilitate vertical product whereas functionalized sources. Collaborative
                morph
                competitive processes via principle-centered bandwidth.</p>
            </div>
          </div>
          <div className="col-12 event-item">
            <div className="image">
              <a href="event-single.html"><Image
                alt="khalid ben"
                src={event} className="img-fluid"
                placeholder="blur"
                quality={100} priority
                style={{
                  minWidth: "100%"
                }}
              /></a>
            </div>
            <div className="time">
              <h2>24</h2>
              <span>. . .</span>
              <p>April</p>
            </div>
            <div className="content">
              <h3>Online Learning and Earning Guidline</h3>
              <ul>
                <li><i className="fa fa-clock-o" aria-hidden="true"></i> 8:00 am</li>
                <li><i className="fa fa-map-marker" aria-hidden="true"></i> New Elefent Road, Dhaka,
                  Bangladesh
                </li>
              </ul>
              <p>Distinctive facilitate vertical product whereas functionalized sources. Collaborative
                morph
                competitive processes via principle-centered bandwidth.</p>
            </div>
          </div>
          <div className="text-center mt-4">
            <div>
              <a className="btn btn-primary btn-lg shadow-primary col-12 col-xl-3 col-lg-3 col-sm-12" href="events.html">View All
                Events</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
