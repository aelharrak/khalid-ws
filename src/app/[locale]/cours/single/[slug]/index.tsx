import course_1 from '../../../../../public/Course1.jpg'
import {useTranslations} from "next-intl";
import Image from "next/image";
import course_4 from "../../../../../../public/Course4.jpg";
import React from "react";

export default function Index() {
  const t = useTranslations("Index");
  return <section className="container mt-4 mb-lg-5 pt-lg-2 pb-5">
    <article className="card border-0 shadow-sm overflow-hidden mb-4">
      <div className="row g-0">
        <div className="col-sm-4 position-relative bg-position-center bg-repeat-0 bg-size-cover"
             style={{
               // use the src property of the image object
               backgroundImage: `url(${course_4.src})`,
               backgroundPosition: "center",
               backgroundSize: "cover",
               backgroundRepeat: "no-repeat",
               minHeight: "15rem"
             }}
        >
          <a href="blog-single.html" className="position-absolute top-0 start-0 w-100 h-100"
             aria-label="Read more"></a>
          {/*<a href="#"
             className="btn btn-icon btn-light bg-white border-white btn-sm rounded-circle position-absolute top-0 end-0 zindex-5 me-3 mt-3"
             data-bs-toggle="tooltip" data-bs-placement="left" aria-label="Read later"
             data-bs-original-title="Read later">
            <i className="bx bx-bookmark"></i>
          </a>*/}
        </div>
        <div className="col-sm-8">
          <div className="card-body">
            <div className="d-flex align-items-center mb-3">
              <a href="#"
                 className="badge fs-sm text-nav bg-secondary text-decoration-none">Processes &amp; Tools</a>
              <span className="fs-sm text-muted border-start ps-3 ms-3">Sep 3, 2023</span>
            </div>
            <h3 className="h4">
              <a href="blog-single.html">5 Bad Landing Page Examples &amp; How We Would Fix Them</a>
            </h3>
            <p>Tellus sagittis dolor pellentesque vel porttitor magna aliquet arcu. Interdum risus mauris pulvinar
              et vel. Morbi tellus, scelerisque vel metus. Scelerisque arcu egestas ac commodo, ac nibh. Pretium ac
              elit sed nulla nec.</p>
            <hr className="my-4"/>
            <div className="d-flex align-items-center justify-content-between">
              <a href="#" className="d-flex align-items-center fw-bold text-dark text-decoration-none me-3">
                <Image src={course_4}
                       className="rounded-circle me-3" width="48" alt="Avatar"/>
              </a>
              <div className="d-flex align-items-center text-muted">
                <div className="d-flex align-items-center me-3">
                  <i className="bx bx-like fs-lg me-1"></i>
                  <span className="fs-sm">8</span>
                </div>
                <div className="d-flex align-items-center me-3">
                  <i className="bx bx-comment fs-lg me-1"></i>
                  <span className="fs-sm">7</span>
                </div>
                <div className="d-flex align-items-center">
                  <i className="bx bx-share-alt fs-lg me-1"></i>
                  <span className="fs-sm">4</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article>
  </section>
}
