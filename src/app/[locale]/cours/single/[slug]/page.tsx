import Image from "next/image";
import image from '../../../../../../public/IMG_0305.png'

import React from "react";
import Index from './index'
import course_4 from "../../../../../../public/Course2.jpg";
import Slider2 from "../../slider/slider2";
import Tags from './tags'

export default function Page() {
  return <>
    <section className="bg-dark pt-2 pt-lg-3 pb-lg-5 z-0 position-relative" data-bs-theme="dark"
             style={{
               // use the src property of the image object
               backgroundImage: `url(${course_4.src})`,
               backgroundPosition: "center",
               backgroundSize: "cover",
               backgroundRepeat: "no-repeat",
               minHeight: "15rem",
               overflow: "hidden"
             }}>

      <span className="position-absolute top-0 start-0 w-100 h-100 bg-dark opacity-70"></span>
      <div className="container position-relative zindex-5 pb-5">
        <nav className="py-4" aria-label="breadcrumb">
          <ol className="breadcrumb mb-0">
            <li className="breadcrumb-item">
              <a href="landing-online-courses.html"><i className="bx bx-home-alt fs-lg me-1"></i>Home</a>
            </li>
            <li className="breadcrumb-item">
              <a href="portfolio-courses.html">Courses</a>
            </li>
            <li className="breadcrumb-item active" aria-current="page">Fullstack Web Developer Course from Scratch</li>
          </ol>
        </nav>


        <Tags/>

        <h1>Fullstack Web Developer Course from Scratch</h1>
        <p className="fs-lg text-light opacity-70">Egestas feugiat lorem eu neque suspendisse ullamcorper scelerisque
          aliquam mauris.</p>
        <div className="d-sm-flex py-3 py-md-4 py-xl-5">
          <div className="d-flex border-sm-end pe-sm-3 me-sm-3 mb-2 mb-sm-0">
            <div className="text-nowrap me-1">
              <i className="bx bxs-star text-warning"></i>
              <i className="bx bxs-star text-warning"></i>
              <i className="bx bxs-star text-warning"></i>
              <i className="bx bxs-star text-warning"></i>
              <i className="bx bx-star text-muted opacity-75"></i>
            </div>
            <span className="text-light opacity-70">(1.2K reviews)</span>
          </div>
          <div className="d-flex border-sm-end pe-sm-3 me-sm-3 mb-2 mb-sm-0">
            <i className="bx bx-like fs-xl text-light opacity-70 me-1"></i>
            <span className="text-light opacity-70">4.2K likes</span>
          </div>
          <div className="d-flex">
            <i className="bx bx-time fs-xl text-light opacity-70 me-1"></i>
            <span className="text-light opacity-70">220 hours</span>
          </div>
        </div>
        <div className="d-flex align-items-center mt-xl-5 pt-2 pt-md-4 pt-lg-5">
          <Image src={image} className="rounded-circle" width="60" alt="Albert Flores"/>
          <div className="ps-3">
            <div className="text-light opacity-80 mb-1">Created by</div>
            <h6>khalid</h6>
          </div>
        </div>

      </div>
    </section>
    {/*Course description*/}
    <section className="container pt-5 mt-2 mt-lg-4 mt-xl-5">
      <div className="row">
        {/*Sidebar (Course summary)*/}
        <aside className="col-lg-4 col-md-5 offset-xl-1 order-md-2 mb-5">
          <div className="position-sticky top-0 pt-5">
            <div className="pt-5 mt-md-3">
              <div className="card shadow-sm p-sm-3">
                <div className="card-body">
                  <h4 className="mb-4">This course includes:</h4>
                  <ul className="list-unstyled">
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                      <span className="h6 fw-light mb-0"><i className="fas fa-fw fa-book-open text-primary"></i> &nbsp;Lectures</span>
                      <span>30</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                      <span className="h6 fw-light mb-0"><i
                        className="fas fa-fw fa-clock text-primary"></i>&nbsp;Duration</span>
                      <span>4h 50m</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                      <span className="h6 fw-light mb-0"><i
                        className="fas fa-fw fa-signal text-primary"></i>&nbsp;Skills</span>
                      <span>Beginner</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                      <span className="h6 fw-light mb-0"><i
                        className="fas fa-fw fa-globe text-primary"></i>&nbsp;Language</span>
                      <span>English</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                      <span className="h6 fw-light mb-0"><i className="fas fa-fw fa-user-clock text-primary"></i>&nbsp;Deadline</span>
                      <span>Nov 30 2021</span>
                    </li>
                    <li className="list-group-item d-flex justify-content-between align-items-center">
                      <span className="h6 fw-light mb-0"><i className="fas fa-fw fa-medal text-primary"></i>&nbsp;Certificate</span>
                      <span>Yes</span>
                    </li>
                  </ul>
                 {/* <div className="h2 d-flex align-items-center mb-4">$28.99
                    <del className="text-muted fs-xl fw-normal ms-2">49.99</del>
                  </div>*/}
                  <div className="d-grid gap-2">
                    <a href="/cours/detail" className="btn btn-primary btn-lg shadow-primary" type="button">Join the course</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </aside>

        {/*Content*/}
        <div className="col-xl-7 col-lg-8 col-md-7 order-md-1 mb-5">
          <h2 className="h1 pb-md-2 pb-lg-3">Course description</h2>
          <p className="pb-4 mb-3">Suspendisse natoque sagittis, consequat turpis. Sed tristique tellus morbi magna. At
            vel senectus accumsan, arcu mattis id tempor. Tellus sagittis, euismod porttitor sed tortor est id. Feugiat
            velit velit, tortor ut. Ut libero cursus nibh lorem urna amet tristique leo. Viverra lorem arcu nam nunc at
            ipsum quam. A proin id sagittis dignissim mauris condimentum ornare. Tempus mauris sed dictum ultrices.</p>
          <h3 className="mb-4">What you'll learn</h3>
          <ul className="list-unstyled mb-5">
            <li className="d-flex align-items-center mb-2">
              <i className="bx bx-check-circle text-primary fs-xl me-2"></i>
              Sed lectus donec amet eu turpis interdum.
            </li>
            <li className="d-flex align-items-center mb-2">
              <i className="bx bx-check-circle text-primary fs-xl me-2"></i>
              Nulla at consectetur vitae dignissim porttitor.
            </li>
            <li className="d-flex align-items-center mb-2">
              <i className="bx bx-check-circle text-primary fs-xl me-2"></i>
              Phasellus id vitae dui aliquet mi.
            </li>
            <li className="d-flex align-items-center mb-2">
              <i className="bx bx-check-circle text-primary fs-xl me-2"></i>
              Integer cursus vitae, odio feugiat iaculis aliquet diam, et purus.
            </li>
            <li className="d-flex align-items-center mb-2">
              <i className="bx bx-check-circle text-primary fs-xl me-2"></i>
              In aenean dolor diam tortor orci eu.
            </li>
          </ul>

          <h2 className="h1 pt-md-2 pt-lg-4 pt-xl-5 pb-md-3 pb-lg-4 mb-md-4">Program details</h2>
          <div className="steps steps-sm">
            <div className="step">
              <div className="step-number">
                <div className="step-number-inner">1</div>
              </div>
              <div className="step-body">
                <h4 className="mb-2">Introduction. Getting started</h4>
                <p className="mb-0">Nulla faucibus mauris pellentesque blandit faucibus non. Sit ut et at suspendisse
                  gravida hendrerit tempus placerat.</p>
              </div>
            </div>
            <div className="step">
              <div className="step-number">
                <div className="step-number-inner">2</div>
              </div>
              <div className="step-body">
                <h4 className="mb-2">The ultimate HTML developer: advanced HTML</h4>
                <p className="mb-0">Lobortis diam elit id nibh ultrices sed penatibus donec. Nibh iaculis eu sit cras
                  ultricies. Nam eu eget etiam egestas donec scelerisque ut ac enim. Vitae ac nisl, enim nec accumsan
                  vitae est.</p>
              </div>
            </div>
            <div className="step">
              <div className="step-number">
                <div className="step-number-inner">3</div>
              </div>
              <div className="step-body">
                <h4 className="mb-2">CSS &amp; CSS3: basic</h4>
                <p className="mb-0">Duis euismod enim, facilisis risus tellus pharetra lectus diam neque. Nec ultrices
                  mi faucibus est. Magna ullamcorper potenti elementum ultricies auctor.</p>
              </div>
            </div>
            <div className="step">
              <div className="step-number">
                <div className="step-number-inner">4</div>
              </div>
              <div className="step-body">
                <h4 className="mb-2">JavaScript basics for beginners</h4>
                <p className="mb-0">Morbi porttitor risus imperdiet a, nisl mattis. Amet, faucibus eget in platea vitae,
                  velit, erat eget velit. At lacus ut proin erat.</p>
              </div>
            </div>
            <div className="step">
              <div className="step-number">
                <div className="step-number-inner">5</div>
              </div>
              <div className="step-body">
                <h4 className="mb-2">Understanding APIs</h4>
                <p className="mb-0">Risus morbi euismod in congue scelerisque fusce pellentesque diam consequat. Nisi
                  mauris nibh sed est morbi amet arcu urna. Malesuada feugiat quisque consectetur elementum diam vitae.
                  Dictumst facilisis odio eu quis maecenas risus odio fames bibendum ullamcorper.</p>
              </div>
            </div>
            <div className="step">
              <div className="step-number">
                <div className="step-number-inner">6</div>
              </div>
              <div className="step-body">
                <h4 className="mb-2">Python from beginner to advanced</h4>
                <p className="mb-0">Quis risus quisque diam diam. Volutpat neque eget eu faucibus sed urna fermentum
                  risus. Est, mauris morbi nibh massa.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    {/*Related courses (Carousel on narrow screens)*/}
    <Slider2 title="More courses like this"/>
  </>
}
