import React from "react";

export default function Item() {
  return (  <div className="d-flex pt-3 pb-4 py-sm-4 pt-lg-5">
    <span className="badge bg-success fs-sm me-2">Best Seller</span>
    <a href="#" className="badge bg-white text-nav fs-sm text-decoration-none">Web Development</a>
  </div>)
}
