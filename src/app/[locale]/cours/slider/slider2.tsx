'use client'
import Image from 'next/image'
import {images} from './lib/images'

import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import {Swiper, SwiperSlide} from 'swiper/react'
import image from "../../../../../public/Course3.jpg";
import React from "react";
import Item from "./item";
import {Autoplay, EffectFade, Navigation, Pagination} from 'swiper/modules';

export default function Page({title}) {
  return (
      <section className="bg-1 pt-5 pb-4 pb-lg-5">
          <div className="container courses py-2 pt-lg-4 pb-lg-0">
              <h2 className="h1 text-center pb-4">{title}</h2>
            {/*Slider main container*/}
              <div
                  className="row py-5 g-4 justify-content-center">
                {/*Item*/}
                <Swiper
                  watchSlidesProgress={true} slidesPerView={1} className="mySwiper"
                  pagination={{
                    clickable: true,
                  }}
                  breakpoints={{
                  '@0.00': {
                    slidesPerView: 1,
                    spaceBetween: 10,
                  },
                  '@0.75': {
                    slidesPerView: 2,
                    spaceBetween: 10,
                  },
                  '@1.00': {
                    slidesPerView: 3,
                    spaceBetween: 10,
                  },
                  '@1.50': {
                    slidesPerView: 3,
                    spaceBetween: 10,
                  },
                }} modules={[Pagination]} 
                >
                  {images.map((image_, index) => (
                    <SwiperSlide
                      key={index}>

                      {/*<Item price="free"
                            image={image}/>*/}

                      <article className="card h-100 border-0 shadow-sm mx-2">
                        <div className="position-relative">
                          <a href="#" className="d-block position-absolute w-100 h-100 top-0 start-0"
                             aria-label="Course link"></a>
                          <span className="badge bg-danger position-absolute top-0 start-0 zindex-2 mt-3 ms-3">Sale!</span>
                          <a href="#"
                             className="btn btn-icon btn-light bg-white border-white btn-sm rounded-circle position-absolute top-0 end-0 zindex-2 me-3 mt-3"
                             data-bs-toggle="tooltip" data-bs-placement="left" aria-label="Save to Favorites"
                             data-bs-original-title="Save to Favorites">
                            <i className="bx bx-bookmark"></i>
                          </a>
                          <Image src={image} className="card-img-top" alt="Albert Flores"/>
                        </div>
                        <div className="card-body pb-3">
                          <h3 className="h5 mb-2">
                            <a href="#">HTML, CSS, JavaScript Web Developer</a>
                          </h3>
                          <p className="fs-sm mb-2">By Robert Fox</p>
                          <p className="text-muted mb-0"><span className="fs-lg fw-semibold text-danger me-2">$9.99</span>
                            <del>$44.99</del>
                          </p>
                        </div>
                        <div className="card-footer d-flex align-items-center fs-sm text-muted py-4">
                          <div className="d-flex align-items-center me-4">
                            <i className="bx bx-time fs-xl me-1"></i>
                            210 hours
                          </div>
                          <div className="d-flex align-items-center">
                            <i className="bx bx-like fs-xl me-1"></i>
                            98% (2.7K)
                          </div>
                        </div>
                      </article>
                    </SwiperSlide>
                  ))}
                </Swiper>
              </div>
          </div>
      </section>



  )
}
