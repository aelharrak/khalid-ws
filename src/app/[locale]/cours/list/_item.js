"use client";
import Image from "next/image";
import React from "react";

export default function Item({image, price}) {
  return (
    <div className="col-xl-4 col-lg-6 col-sm-6">
      <div className="course-item">
        <div className="image">
          <a href="/cours/single/33">
            <Image
              alt="khalid ben"
              src={image}
              placeholder="blur"
              quality={100}

              style={{
                objectFit: 'cover',
                width: '100%'
              }}
            />
          </a>
        </div>
        <div className="content">
          <h4><a href="/cours/single/33">Basic English Spoken and Writing</a></h4>
          <p>Explore the architecture of Yeoman to help you build web applications.</p>
          <ul className="info">
            <li>
              <p>Students<span>:</span></p>200 Seats
            </li>
            <li>
              <p>Duration<span>:</span></p>54m 26s
            </li>
          </ul>
        </div>
        <div className="content-bottom">
          <a href="/cours/single/33">Start course</a>
          <span>{price}</span>
        </div>
      </div>
    </div>
  );
}
