"use client";
import Image from "next/image";
import React from "react";

export default function Item({image, price}) {
  return (
    <div className="col-xl-3 col-lg-6 col-sm-6 col-2">
      <div className="course-item">
        <div className="image">
          <a href="/cours/single/33">
            <Image
              alt="khalid ben"
              src={image}
              placeholder="blur"
              quality={100}
              //height="200px"
              style={{
                objectFit: 'cover',
                width: '100%', height: "200px"
              }}
            />
          </a>
        </div>
        <div className="content">
          <h4><a href="/cours/single/33">Basic English Spoken and Writing</a></h4>
          <ul className="info">
            <li>
              <p>Students<span>:</span></p>200 Seats
            </li>
            <li>
              <p>Duration<span>:</span></p>3 Months
            </li>
          </ul>
        </div>
        <div className="content-bottom">
          <a href="instructor.html">English</a>
          <span>{price}</span>
        </div>
      </div>
    </div>
  );
}
