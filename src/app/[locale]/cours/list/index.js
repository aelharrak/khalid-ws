import course_1 from '../../../../../public/Course1.jpg'
import course_2 from '../../../../../public/Course2.jpg'
import course_3 from '../../../../../public/Course3.jpg'
import course_4 from '../../../../../public/Course4.jpg'
import {useTranslations} from "next-intl";
import Item from "./item";

export default function CourseList({title, bgColorCssClass}) {
  const t = useTranslations("Index");
  return (
    <section className={bgColorCssClass}>
      <div className="container">
        <h3>{t(title)}</h3>
        <div className="row py-5 g-4 justify-content-center">
          <Item price="free"
                image={course_1}/>
          <Item price="$100"
                image={course_2}/>
          <Item price="$345"
                image={course_3}/>
          <Item price="free"
                image={course_4}/>
        </div>
      </div>
    </section>
  );
}
