"use client";
import Image from "next/image";
import React from "react";

export default function Item({image, price}) {
  return (
    <div className="col-xl-3 col-lg-6 col-sm-6">
      <div className="card shadow h-100">

        <Image
          alt="khalid ben"
          src={image}
          placeholder="blur"
          quality={100}
          className="card-img-top"
          style={{
            objectFit: 'cover',
            width: '100%'
          }}
        />
        <div className="card-body pb-0">
          <div className="d-flex justify-content-between mb-2">
            <a href="#" className="badge bg-primary bg-opacity-10 text-primary">All level</a>
            <a href="#" className="text-dark"><i className="far fa-heart"></i></a>
          </div>
          <h5 className="card-title fw-normal"><a href="#">Sketch from A to Z: for app designer</a></h5>
          <p className="mb-2 text-truncate-2">Proposal indulged no do sociable he throwing settling..</p>
          <ul className="list-inline mb-0">
            <li className="list-inline-item me-0 small"><i className="fas fa-star text-warning"></i></li>
            <li className="list-inline-item me-0 small"><i className="fas fa-star text-warning"></i></li>
            <li className="list-inline-item me-0 small"><i className="fas fa-star text-warning"></i></li>
            <li className="list-inline-item me-0 small"><i className="fas fa-star text-warning"></i></li>
            <li className="list-inline-item me-0 small"><i className="far fa-star text-warning"></i></li>
            <li className="list-inline-item ms-2 text-dark">4.0/5.0</li>
          </ul>
        </div>
        <div className="card-footer pt-0 pb-3">
          <hr/>
          <div className="d-flex justify-content-between">
            <span className="text-dark"><i className="far fa-clock text-danger me-2"></i>12h 56m</span>
            <span className="text-dark"><i className="fas fa-table text-orange me-2"></i>15 lectures</span>
          </div>
        </div>
      </div>
    </div>
  );
}
