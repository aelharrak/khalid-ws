import {useTranslations} from "next-intl";

export default function LanguageSwitcher() {
  const t = useTranslations("menu");

  return (
    <div className="mt-3 mt-lg-0 d-flex align-items-center">
      <div className="navbar-nav ms-2">
        <button className="btn btn-sm btn-dark mb-0"><i className="bi bi-power me-2"></i>Sign In</button>
      </div>
    </div>
  )
}
