import Link from "next/link";
import {useTranslations} from "next-intl";
import LanguageSwitcher from "./LanguageSwitcher";

const Navbar = ({lang}) => {
  const t = useTranslations("menu");
  return (
    <nav className="navbar navbar-expand-lg  ftco_navbar ftco-navbar-light
    navbar-expand-lg navbar-light w-100" id="ftco-navbar">
      <div className="container">
        <Link className="navbar-brand" href="/">Bioenergy <span>Hacking</span></Link>
        <button className="navbar-toggler p-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#bdSidebar"
                aria-controls="bdSidebar" aria-label="Toggle docs navigation">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" className="bi" fill="currentColor"
               viewBox="0 0 16 16">
            <path fillRule="evenodd"
                  d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"></path>
          </svg>

          <span className="d-none fs-6 pe-1">Browse</span>
        </button>

        <div className="offcanvas offcanvas-start offcanvas-nav" id="bdSidebar" aria-label="navigation menu ">
          <ul className="navbar-nav mx-auto align-items-lg-center">
            <li className="nav-item"><Link className="nav-link" href="/">{t("home")}</Link></li>
            <li className="nav-item"><Link className="nav-link" href="/about">{t("about")}</Link></li>
            <li className="nav-item"><Link className="nav-link" href="/plan">{t("sublist")}</Link></li>
            <li className="nav-item"><Link className="nav-link" href="/faq">{t("faq")}</Link></li>
            <li className="nav-item"><Link className="nav-link" href="/contact">{t("contact")}</Link></li>
          </ul>
        </div>
        <LanguageSwitcher/>
      </div>
    </nav>
  );
};
export default Navbar;
