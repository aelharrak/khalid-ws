import Link from "next/link";
import {useTranslations} from "next-intl";
import LanguageSwitcher from "./LanguageSwitcher";
import Image from "next/image";
import * as React from "react";
import logo from '../../../../public/assets/images/logo.svg'

const Navbar = ({lang}) => {
  const t = useTranslations("menu");
  return (
    <header className="navbar-light navbar-sticky">
      <nav className="navbar navbar-expand-lg">
        <div className="container">
     {/*     <Link className="navbar-brand me-0" href='/'>
            <Image className="light-mode-item navbar-brand-item" width={150} src={logo} alt="Logo"/>
          </Link>
*/}
          <Link className="navbar-brand me-0 fs-5 lh-1" href="/">Bioenergy <span className="my-0 py-0 fs-6">Hacking</span></Link>
          <button className="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse"
                  data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                  aria-label="Toggle navigation">
            <span className="navbar-toggler-animation">
              <span></span>
              <span></span>
              <span></span>
            </span>
          </button>
          <div className="offcanvas offcanvas-start offcanvas-nav" id="bdSidebar" aria-label="navigation menu ">
            <ul className="navbar-nav mx-auto align-items-lg-center">
              <li className="nav-item"><Link className="nav-link" href="/">{t("home")}</Link></li>
              <li className="nav-item"><Link className="nav-link" href="/about">{t("about")}</Link></li>
              <li className="nav-item"><Link className="nav-link" href="/plan">{t("sublist")}</Link></li>
              <li className="nav-item"><Link className="nav-link" href="/faq">{t("faq")}</Link></li>
              <li className="nav-item"><Link className="nav-link" href="/contact">{t("contact")}</Link></li>
            </ul>
          </div>
          <LanguageSwitcher/>
        </div>
      </nav>
    </header>
  );
};
export default Navbar;
