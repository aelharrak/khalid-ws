import {useTranslations} from "next-intl";
import Image from 'next/image'
import CourseList from "./cours/list";
import imageProfile2 from '../../../public/khalid-2.png'
import Event from "./event";
import Link from 'next/link'
// Import Swiper styles
import 'swiper/css';
import * as React from "react";
import dynamic from 'next/dynamic'

export const metadata = {
  title: 'mon titre ici !!!',
}

const Slider = dynamic(() => import('./slider'), {
  loading: () => <p>Loading...</p>,
});

export default function Home() {

  const t = useTranslations("Index");
  return (<main>

      <div className="row g-0">
        <div className="col-12">
          <Slider/>
        </div>
      </div>

      <section className="py-4 py-lg-5">
        <div className="container">
          <div className="row">
            <div className="col-md-6">

              <div className="elementor-column-wrap elementor-element-populated">
                <div className="elementor-widget-wrap">
                  <div
                    className="mb-4 elementor-element elementor-element-c3260f5 poket-star-rating--align-center elementor-widget elementor-widget-witr_section_title"
                    data-id="c3260f5" data-element_type="widget"
                    data-widget_type="witr_section_title.default">
                    <div>
                      <div className="witr_section_title">
                        <div className="witr_section_title_inner text-left">
                          <h3>We Create Result-Oriented
                            Dynamic Applications</h3>

                          <p>
                            I discovered my healing abilities twenty years ago. At the time, I thought there was
                            something
                            wrong with me. I found out later that many people have a similar gift, so I worked hard to
                            develop my energy skills. With the help of God, I learned many energy techniques and created
                            my own healing method with the guidance of my teacher Janet Amare. One way that is more
                            effective and precise. I call it bioenergy Hacking.
                          </p><h3>Bioenergy hacking</h3>
                          <p> is a revolutionary brain technique developed by <Link className="" href="/about">Khalid
                            Benhammou</Link>, an energy healer and coach.
                          </p>
                          <p>
                            This technique is designed to help people to clear themselves from fear, anger,
                            depression...
                            or any emotional pains by accessing their subconscious mind and hacking their brains through
                            the visualization techniques.
                            This technique can be helpful for psychologists, coaches, and social workers...
                          </p>

                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="elementor-column-wrap elementor-element-populated">
                <div className="elementor-widget-wrap">
                  <div
                    className="elementor-element elementor-element-df987ca poket-star-rating--align-center elementor-widget elementor-widget-witr_section_s_image"
                    data-id="df987ca" data-element_type="widget"
                    data-widget_type="witr_section_s_image.default">
                    <div>

                      <div className="single_image_area">
                        <div className="single_image single_line_option  ">
                          <Image
                            alt="khalid ben"
                            src={imageProfile2}
                            placeholder="blur" sizes="100vw"
                            quality={100} priority
                            style={{
                              objectFit: 'cover',
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <CourseList title="freeCourse" bgColorCssClass="bg-light py-4 py-lg-5  courses"/>
      <CourseList title="featuredCourses" bgColorCssClass="py-4 py-lg-5  courses"/>
      <Event title="upcomingEvents" bgColorCssClass="bg-1 py-4 py-lg-5"/>

      <section className="bg-2 py-4 py-lg-5 newsletter">
        <div className="container">
          <div className="section__header text-center">
            <h2 className="section__title">Stay connected with me to keep yourself updated</h2>
            <p>Subscribe to our newsletter to get great offers and updates!</p>
          </div>
          <form action="#" method="post" autoComplete="off">
            <div className="newsletter__inputs">
              <input type="email" name="newsletter__mail" id="newsletterMail" placeholder="Your Email Address"
                     required/>
              <button type="submit" className="btn btn-primary cmn-butto">Subscribe</button>
            </div>
          </form>
        </div>
      </section>
    </main>
  );
}
