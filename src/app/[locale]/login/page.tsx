"use client";
import './style.css'
import * as React from "react";
import {useState} from "react";
import {SubmitHandler, useForm} from "react-hook-form"
import {redirect} from "next/dist/server/api-utils";

interface IFormInput {
  email: string
  password: string
}

export default function Index() {

  const {register, formState: {errors}, handleSubmit, watch} = useForm<IFormInput>()
  const [cssContainer, setCssContainer] = useState('container  bg-white ');
  const onSubmit:  SubmitHandler<IFormInput> = async (data) => {
    try {
      const res = await fetch('/api/register', {
        method:'POST',
        body: JSON.stringify(data),
        headers: {
          "Content-Type":'application/json'
        }
      })

      if(res.ok) {
        console.log('ok')
        window.location.href = '/'
      } else {
        console.log('ko')
      }

    } catch (e) {
      console.error(e)
    }
    //console.log(data)


  }





  return (
    <div className="position-fixed h-100 top-0 bg-white w-100 z-3 ">
      <div className={cssContainer} id="container">
        <div className="form-container sign-up-container">
          <form onSubmit={handleSubmit(onSubmit)} method="POST">
            <h1>Create Account</h1>
            <span>or use your email for registration</span>
            <input type="text" placeholder="Name"/>
            <input type="email" placeholder="Email"/>
            <input type="password" placeholder="Password"/>
            <button>Sign Up</button>
            <a onClick={() => setCssContainer('container bg-white')}
               className="btn btn-primary" id="signUp">Sign In
            </a>
          </form>
        </div>

        <div className="form-container position-absolute sign-in-container">
            <div className="alert alert-secondary alert-dismissible fade show" role="alert">
            A simple dark alert—check it out!
            <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>

          <form className="needs-validation" noValidate onSubmit={handleSubmit(onSubmit)} method="POST">
            <h1>Sign in</h1>




            <div className="form-floating mb-3">

              <input
                     {...register('email', {
                       required: 'Email is required',
                       pattern: {
                         value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                         message: 'Please enter a valid email',
                       },
                     })}
                     aria-invalid={errors.email ? "true" : "false"}
                     type="text"
                     className="form-control  is-invalid"/>
              <label
                htmlFor="email"
                className=""
              >
                Email address
              </label>
              {errors.email && <p role="alert">{errors.email.message}</p>}
            </div>
            <div className="form-floating mb-3">
              <input
                id="signin-password"
                type="password"

                {...register('password', {
                  required: 'password is required',
                })}
                className="form-control"
              /> <label
              htmlFor="password"
              className=""
            >
              Password
            </label>
              {errors.password && <span role="alert">{errors.password.message}</span>}
            </div>
            <a href="#">Forgot your password?</a>
            <button type="submit" className="btn btn-dark">Sign In</button>
            <a onClick={() => setCssContainer('container bg-white right-panel-active')}
               className="btn d-none d-sm-block d-md-none btn-primary" id="signUp">Sign Up
            </a>
          </form>
        </div>




        <div className="overlay-container">
          <div className="overlay">
            <div className="overlay-panel overlay-left">
              <h1>Welcome Back!</h1>
              <p>
                To keep connected with us please login with your personal info
              </p>
              <button onClick={() => setCssContainer('container bg-white ')} className="ghost" id="signIn">Sign In
              </button>
            </div>
            <div className="overlay-panel overlay-right">
              <h1>Hello, Friend!</h1>
              <p>Enter your personal details and start journey with us</p>
              <button onClick={() => setCssContainer('container bg-white right-panel-active')}
                      className="ghost" id="signUp">Sign Up
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>


  )
}
