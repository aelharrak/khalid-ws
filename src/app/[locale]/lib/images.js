// Images
import first from '../../../../public/banner-top.jpeg'
import second from '../../../../public/banner-top.jpeg'
import third from '../../../../public/banner-top.jpeg'
import fourth from '../../../../public/banner-top.jpeg'

export const images = [
  { src: first, alt: 'First' },
  { src: second, alt: 'Second' },
  { src: third, alt: 'Third' },
  { src: fourth, alt: 'Fourth' }
]
