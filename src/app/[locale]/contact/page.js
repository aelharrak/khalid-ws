"use client";
import React, {useState} from "react";
import img from '../../../../public/video-bg.jpg'

const initValues = {name: "", email: "", subject: "", message: ""};
const initState = {isLoading: false, error: "", values: initValues};

/*export const sendContactForm = async (data) =>
  fetch("/api/contact", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {"Content-Type": "application/json", Accept: "application/json"},
  }).then((res) => {
    if (!res.ok) throw new Error("Failed to send message");
    return res.json();
  });*/

const sendContactForm = async (data) =>
  fetch("/api/contact", {
    method: "GET",
  }).then((res) => {
    return res.json();
  });

export default function ContactUs() {

  const [state, setState] = useState(initState);
  const [touched, setTouched] = useState({});
  const {values, isLoading, error} = state;

  const onBlur = ({target}) =>
    setTouched((prev) => ({...prev, [target.name]: true}));

  const handleChange = ({target}) =>
    setState((prev) => ({
      ...prev,
      values: {
        ...prev.values,
        [target.name]: target.value,
      },
    }));
  const onSubmit = async () => {
    setState((prev) => ({
      ...prev,
      isLoading: true,
    }));
    try {
      await sendContactForm(values);
      setTouched({});
      setState(initState);
      console.log('ok')
    } catch (error) {
      console.error('error')
    }
  };
  return (
    <>
      <section className="home-page-contact-us-bg-wrapper" style={{backgroundImage: `url(${img.src})`}}>
        <div className="container">
          <div className="row">
            <div className="video-part">
              <div className="video-overlay witr_all_color_v">
                <div className="video-item   witr_none_before   text-center">
                  <a href="https://youtu.be/BS4TUd7FJSg">
                    <i className="icofont-ui-play"></i>
                  </a>
                  <h3>Our Purpose is to Create Your Own <br/> Web Masterpiece</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="container p-md-0 my-5">
        <div className="">
          <div className="">
            <div className="" data-id="ba0d31c" data-element_type="column">
              <div className="elementor-column-wrap elementor-element-populated">
                <div className="elementor-widget-wrap">
                  <div>
                    <div>

                      <div className="apartment_area">

                        <div className="witr_apartment_form">
                          <div className="wpcf7 js" id="wpcf7-f235-p9-o1" lang="en-US"
                               dir="ltr">
                            <form className="wpcf7-form init" aria-label="Contact form"
                                  noValidate="novalidate" data-status="init">
                              <div className="row">
                                <div className="form-floating mb-3">
                                  <input type="text" className="form-control" id="name"
                                         placeholder="name"/>
                                  <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                  <input type="email" className="form-control" id="email"
                                         placeholder="email"/>
                                  <label htmlFor="email">Your Email</label>
                                </div>

                                <div className="form-floating mb-3">
                                  <input type="text" className="form-control" id="subject"
                                         placeholder="subject"/>
                                  <label htmlFor="subject">Your Subject</label>
                                </div>
                                <div className="form-floating mb-3">
                                  <textarea className="form-control" placeholder="Leave a comment here"
                                            id="floatingTextarea2" cols={3}></textarea>
                                  <label htmlFor="floatingTextarea2">Text Message</label>
                                </div>


                                <div className="col-lg-12 col-md-12">
                                  <p>
                                    <button
                                      onClick={onSubmit}
                                      type="submit" className="btn btn-primary">Send Message
                                    </button>
                                  </p>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>)
}
