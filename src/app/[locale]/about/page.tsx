import React from "react";
import {useTranslations} from "next-intl";

//import img from '../../../../public/IMG_0305.png'

export const metadata = {
  title: 'About us !!!',
}

const AboutPage = () => {
  const t = useTranslations("menu");
  return <section className="home-banner"
                //  style={{ backgroundImage: `url(${img.src})`}}
      >
    <div className="relative">
      <div className="home-banner-text-wrapper">
        <h1 className="mb-5">Working together for your better life!</h1>
        <p>
          The project is based on the belief in the ability of each person
        </p>
        <p>
          Being professional isn’t about boring people with endless slides and regurgitated
          information. Instead, it’s about putting people at ease, allowing them to speak freely about
          their aspirations
        </p>

        <div className="my-4">
          178 West 27th Street, Suite 527, <br/>
          New York NY 10012, United States
        </div>

        <a className="mb-3" href="mailto:example@demolink.org">
          <span className="elementor-button-text">example@demolink.org</span>
        </a>

        <h2>
          <i className="fa-solid fa-phone"></i>
          <span>+1 234 567 8901</span>
        </h2>
        <p>
          Everyday: from 09:00 to 21:00
        </p>
      </div>
    </div>
  </section>

};
export default AboutPage;
