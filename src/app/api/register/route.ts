import {hash} from "bcrypt"
import {NextResponse} from "next/server";
import {error} from "next/dist/build/output/log";

export async function POST(req: Request) {

  try {
    const {email, password} = await req.json();
    const hashed = await hash(password, 12);
    console.log(email, hashed)

    //db save
    return NextResponse.json(
      {
        user: {
          email, password: hashed
        }
      }
    )
  } catch (e) {
    return new NextResponse(JSON.stringify({
      error: error
    }))
  }



}
