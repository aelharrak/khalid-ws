import NextAuth from "next-auth";
import GitHubProvider from "next-auth/providers/github";

require("dotenv").config();

const authOptions = {
  site: process.env.SITE || "http://localhost:3000",

  // Configure one or more authentication providers
  providers: [
    GitHubProvider({
      clientId: process.env.GITHUB_ID,
      clientSecret: process.env.GITHUB_SECRET
    })
  ],

  // A database is optional, but required to persist accounts in a database
  database: process.env.DATABASE_URL,
};

//export default (req, res) => NextAuth(req, res, options);
export default NextAuth(authOptions)

